#! /bin/bash

# 确保脚本在遇到错误时立即退出
set -e
set -o pipefail

source /home/atguigu/bin/run_common.sh
 
for server in "${servers[@]}"; do
    echo --------- $server ----------
    ssh $server "$*"
done