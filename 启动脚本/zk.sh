#!/bin/bash

source /home/atguigu/bin/run_common.sh

for server in "${zookeeper_servers[@]}"; do
	case $1 in
	"start"){
			echo ---------- zookeeper $server 启动 ------------
			ssh $server "/opt/module/zookeeper-3.7.1/bin/zkServer.sh start"
	};;
	"stop"){
			echo ---------- zookeeper $server 停止 ------------    
			ssh $server "/opt/module/zookeeper-3.7.1/bin/zkServer.sh stop"
	};;
	"status"){
			echo ---------- zookeeper $server 状态 ------------    
			ssh $server "/opt/module/zookeeper-3.7.1/bin/zkServer.sh status"
	};;
	esac
done
