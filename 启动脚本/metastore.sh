#!/bin/bash

SERVER_NAME="hadoop101"
HIVE_METASTORE_PID=$(ssh "$SERVER_NAME" "ps -ef | grep -i '[o]rg.apache.hadoop.hive.metastore.HiveMetaStore' | awk '{print \$2}'")

start_metastore() {
    if [ -z "$HIVE_METASTORE_PID" ]; then
        echo "正在启动Hive Metastore..."
        ssh "$SERVER_NAME" "nohup /opt/module/hive/bin/hive --service metastore >/dev/null 2>&1 &"
        echo "Hive Metastore已启动"
    else
        echo "Hive Metastore已经启动"
    fi
}

stop_metastore() {
    if [ -n "$HIVE_METASTORE_PID" ]; then
        echo "正在关闭Hive Metastore..."
        ssh "$SERVER_NAME" "kill $HIVE_METASTORE_PID"
        echo "Hive Metastore已关闭"
    else
        echo "Hive Metastore未启动"
    fi
}

case $1 in
    start)
        start_metastore
        ;;
    stop)
        stop_metastore
        ;;
    *)
        echo "用法: $0 {start|stop}"
        exit 1
        ;;
esac

