#!/bin/bash

source /home/atguigu/bin/run_common.sh

ZOOKEEPER_HOME=/opt/module/zookeeper

for server in "${zookeeper_servers[@]}"; do
	case $1 in
	"start"){
			echo ---------- zookeeper $server 启动 ------------
			ssh $server "${ZOOKEEPER_HOME}/bin/zkServer.sh start"
	};;
	"stop"){
			echo ---------- zookeeper $server 停止 ------------    
			ssh $server "${ZOOKEEPER_HOME}/bin/zkServer.sh stop"
	};;
	"status"){
			echo ---------- zookeeper $server 状态 ------------    
			ssh $server "${ZOOKEEPER_HOME}/bin/zkServer.sh status"
	};;
	esac
done
