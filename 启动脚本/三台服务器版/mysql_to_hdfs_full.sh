#!/bin/bash

DATAX_HOME=/opt/module/datax

# 如果传入日期则do_date等于传入的日期，否则等于前一天日期
if [ -n "$2" ] ;then
    do_date=$2
else
    do_date=`date -d "-1 day" +%F`
fi

#处理目标路径，此处的处理逻辑是，如果目标路径不存在，则创建；若存在，则清空，目的是保证同步任务可重复执行
handle_targetdir() {
  hadoop fs -test -e $1
  if [[ $? -eq 1 ]]; then
    echo "路径$1不存在，正在创建......"
    hadoop fs -mkdir -p $1
  else
    echo "路径$1已经存在"
    
  fi
}

#数据同步
import_data() {
  datax_config=$1
  target_dir=$2

  handle_targetdir $target_dir
  python $DATAX_HOME/bin/datax.py -p"-Dtargetdir=$target_dir" $datax_config
}

#=================================

input_param="$1"

db_name="gmall"

if [[ -z "$input_param" ]]; then
  echo "请输入参数"
  exit 1
fi

#=================================

dir="/opt/module/datax/job/import"

#定义一个数组，装json列表
file_array=()

for file in "$dir"/*.json; do
  filename=$(basename "$file" .json)
  file_array+=("${filename#${db_name}.}")
done

declare -p file_array

found=0

if [[ "$input_param" == "all" ]]; then
  found=1
  for file in "${file_array[@]}"; do
    
	import_data /opt/module/datax/job/import/${db_name}.${file}.json /origin_data/${db_name}/db/${file}_full/$do_date
    
  done
else
  for file in "${file_array[@]}"; do
    if [[ "$file" == "$input_param" ]]; then
      found=1
      
	  import_data /opt/module/datax/job/import/${db_name}.${input_param}.json /origin_data/${db_name}/db/${input_param}_full/$do_date
	  
      break
    fi
  done
fi

if [[ $found -eq 0 ]]; then
  echo "参数错误"
  exit 1
fi

