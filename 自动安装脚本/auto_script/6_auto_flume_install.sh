#!/bin/bash

# 检查是否为root用户
if [[ "$(whoami)" != "root" ]]; then
  echo "请使用root用户运行此脚本。"
  exit 1
fi

# 确保脚本在遇到错误时立即退出
set -e
set -o pipefail

source ./common.sh

#提取flume名称
flume_version1=$(basename "${flume_package_path}" | awk -F'.tar.gz' '{print $1}')

flume_version="flume"

for server in "${flume_servers[@]}"; do
  # 复制安装包
  sshpass -p "${new_user_password}" ssh -o StrictHostKeyChecking=no "${new_user}@${master_server}" "rsync -av ${flume_package_path}  ${new_user}@${server}:${software_directory}"
done

for server in "${flume_servers[@]}"; do
  # 删除已存在的目录
  sshpass -p "${new_user_password}" ssh -o StrictHostKeyChecking=no "${new_user}@${server}" "sudo rm -rf ${module_path}/flume*"
done

# 解压缩安装包
for server in "${flume_servers[@]}"; do
  
  echo "开始解压 ${server}"
  
  sshpass -p "${new_user_password}" ssh -o StrictHostKeyChecking=no "${new_user}@${server}" "tar -zxf ${flume_package_path} -C ${module_path}/"
done

#修改解压后的文件名称
for server in "${flume_servers[@]}"; do
  sshpass -p "${new_user_password}" ssh -o StrictHostKeyChecking=no "${new_user}@${server}" "mv ${module_path}/${flume_version1} ${module_path}/${flume_version}"
done

#修改配置文件  LOG_DIR
for server in "${flume_servers[@]}"; do

  line_number=$(sshpass -p "${new_user_password}" ssh -o StrictHostKeyChecking=no "${new_user}@${server}" "awk '/LOG_DIR/{print NR; exit}' ${module_path}/${flume_version}/conf/log4j2.xml")
  
  sshpass -p "${new_user_password}" ssh -o StrictHostKeyChecking=no "${new_user}@${server}" "sed -i \"${line_number}d\" ${module_path}/${flume_version}/conf/log4j2.xml"
  
  LOG_DIR_TEMP="<Property name=\\\"LOG_DIR\\\">${module_path}/${flume_version}/log</Property>"
  
  sshpass -p "${new_user_password}" ssh -o StrictHostKeyChecking=no "${new_user}@${server}" "sudo sed -i \"${line_number}i${LOG_DIR_TEMP}\" ${module_path}/${flume_version}/conf/log4j2.xml"
  
done

#修改配置文件  <AppenderRef ref=\"LogFile\" \/>
for server in "${flume_servers[@]}"; do

  LogFile_line_number=$(sshpass -p "${new_user_password}" ssh -o StrictHostKeyChecking=no "${new_user}@${server}" "awk '/<AppenderRef ref=\"LogFile\" \/>/{print NR; exit}' ${module_path}/${flume_version}/conf/log4j2.xml")
  
  LogFile_line_number=$((LogFile_line_number + 1))
  
  sshpass -p "${new_user_password}" ssh -o StrictHostKeyChecking=no "${new_user}@${server}" "sudo sed -i \"${LogFile_line_number}i<AppenderRef ref=\\\"Console\\\" \/>\" ${module_path}/${flume_version}/conf/log4j2.xml"
  
done

#创建job目录
for server in "${flume_servers[@]}"; do
  sshpass -p "${new_user_password}" ssh -o StrictHostKeyChecking=no "${new_user}@${server}" "mkdir -p ${module_path}/${flume_version}/job"
done

# 在for循环中实现覆盖文件
for server in "${flume_servers[@]}"; do

  echo "开始复制配置文件 ${server}"

  sshpass -p "${new_user_password}" ssh -o StrictHostKeyChecking=no "${new_user}@${server}" "scp ${new_user}@${master_server}:${autoshell_path}/flume_conf/file_to_kafka.conf ${new_user}@${server}:${module_path}/${flume_version}/job/"
  
  sshpass -p "${new_user_password}" ssh -o StrictHostKeyChecking=no "${new_user}@${server}" "scp ${new_user}@${master_server}:${autoshell_path}/flume_conf/kafka_to_hdfs_db.conf ${new_user}@${server}:${module_path}/${flume_version}/job/"
  
  sshpass -p "${new_user_password}" ssh -o StrictHostKeyChecking=no "${new_user}@${server}" "scp ${new_user}@${master_server}:${autoshell_path}/flume_conf/kafka_to_hdfs_log.conf ${new_user}@${server}:${module_path}/${flume_version}/job/"
  
  sshpass -p "${new_user_password}" ssh -o StrictHostKeyChecking=no "${new_user}@${server}" "scp ${new_user}@${master_server}:${autoshell_path}/flume_conf/gmall-1.0-SNAPSHOT-jar-with-dependencies.jar ${new_user}@${server}:${module_path}/${flume_version}/lib/"
  
done

echo "flume 安装成功"