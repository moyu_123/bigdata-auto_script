#!/bin/bash

source ./common.sh

# 获取版本
maxwell_version1=$(basename "${maxwell_package_path}" | awk -F'.tar.gz' '{print $1}')

maxwell_version="maxwell"

for server in "${maxwell_servers[@]}"; do
  # 复制安装包
  sshpass -p "${new_user_password}" ssh -o StrictHostKeyChecking=no "${new_user}@${master_server}" "rsync -av ${maxwell_package_path}  ${new_user}@${server}:${software_directory}"
done

for server in "${maxwell_servers[@]}"; do
  # 删除已存在的目录
  sshpass -p "${new_user_password}" ssh -o StrictHostKeyChecking=no "${new_user}@${server}" "sudo rm -rf ${module_path}/maxwell*"
done

# 解压缩安装包
for server in "${maxwell_servers[@]}"; do
  
  echo "开始解压 ${server}"
  
  sshpass -p "${new_user_password}" ssh -o StrictHostKeyChecking=no "${new_user}@${server}" "tar -zxf ${maxwell_package_path} -C ${module_path}/"
done

#修改解压后的文件名称
for server in "${maxwell_servers[@]}"; do
  sshpass -p "${new_user_password}" ssh -o StrictHostKeyChecking=no "${new_user}@${server}" "mv ${module_path}/${maxwell_version1} ${module_path}/${maxwell_version}"
done

#修改MySQL配置文件/etc/my.cnf
for server in "${MySQL_servers[@]}"; do
  
  server_id_line_number=$(sshpass -p "${root_password}" ssh -o StrictHostKeyChecking=no "root@${server}" "awk '/server-id/{print NR; exit}' /etc/my.cnf")
  
  if [[ -n "${server_id_line_number}" ]]; then
	  # 使用 rsa_auth_line_number 进行操作，在 sed 命令中删除该行
	  sshpass -p "${root_password}" ssh -o StrictHostKeyChecking=no "root@${server}" "sed -i \"${server_id_line_number}d\" /etc/my.cnf"
  fi
  
  sshpass -p "${new_user_password}" ssh -o StrictHostKeyChecking=no "${new_user}@${server}" "echo 'server-id = 1' | sudo tee -a /etc/my.cnf"
  
  #########
  log_bin_line_number=$(sshpass -p "${root_password}" ssh -o StrictHostKeyChecking=no "root@${server}" "awk '/log-bin/{print NR; exit}' /etc/my.cnf")
  
  if [[ -n "${log_bin_line_number}" ]]; then
	  # 使用 rsa_auth_line_number 进行操作，在 sed 命令中删除该行
	  sshpass -p "${root_password}" ssh -o StrictHostKeyChecking=no "root@${server}" "sed -i \"${log_bin_line_number}d\" /etc/my.cnf"
  fi
  
  sshpass -p "${new_user_password}" ssh -o StrictHostKeyChecking=no "${new_user}@${server}" "echo 'log-bin=mysql-bin' | sudo tee -a /etc/my.cnf"
  
  #########
  binlog_format_line_number=$(sshpass -p "${root_password}" ssh -o StrictHostKeyChecking=no "root@${server}" "awk '/binlog_format/{print NR; exit}' /etc/my.cnf")
  
  if [[ -n "${binlog_format_line_number}" ]]; then
	  # 使用 rsa_auth_line_number 进行操作，在 sed 命令中删除该行
	  sshpass -p "${root_password}" ssh -o StrictHostKeyChecking=no "root@${server}" "sed -i \"${binlog_format_line_number}d\" /etc/my.cnf"
  fi
  
  sshpass -p "${new_user_password}" ssh -o StrictHostKeyChecking=no "${new_user}@${server}" "echo 'binlog_format=row' | sudo tee -a /etc/my.cnf"
  
  #########
  binlog_do_db_line_number=$(sshpass -p "${root_password}" ssh -o StrictHostKeyChecking=no "root@${server}" "awk '/binlog-do-db/{print NR; exit}' /etc/my.cnf")
  
  if [[ -n "${binlog_do_db_line_number}" ]]; then
	  # 使用 rsa_auth_line_number 进行操作，在 sed 命令中删除该行
	  sshpass -p "${root_password}" ssh -o StrictHostKeyChecking=no "root@${server}" "sed -i \"${binlog_do_db_line_number}d\" /etc/my.cnf"
  fi
  
  sshpass -p "${new_user_password}" ssh -o StrictHostKeyChecking=no "${new_user}@${server}" "echo 'binlog-do-db=${binlog_do_db}' | sudo tee -a /etc/my.cnf"
  
done

#重启MySQL服务
for server in "${MySQL_servers[@]}"; do
  sshpass -p "${new_user_password}" ssh -o StrictHostKeyChecking=no "${new_user}@${server}" "sudo systemctl restart mysqld"
done

#创建数据库
for server in "${MySQL_servers[@]}"; do

  sshpass -p "${new_user_password}" ssh -o StrictHostKeyChecking=no "${new_user}@${server}" "mysql -u${mysql_user} -p${mysql_password} -e \"drop database maxwell;\"" 2>/dev/null || true

  sshpass -p "${root_password}" ssh -o StrictHostKeyChecking=no "root@${server}" "mysql -u${mysql_user} -p${mysql_password} -e \"CREATE DATABASE maxwell;\"" 2>/dev/null || true
  
  sshpass -p "${new_user_password}" ssh -o StrictHostKeyChecking=no "${new_user}@${server}" "mysql -u${mysql_user} -p${mysql_password} -e \"drop USER maxwell;\"" 2>/dev/null || true
  
  sshpass -p "${root_password}" ssh -o StrictHostKeyChecking=no "root@${server}" "mysql -u${mysql_user} -p${mysql_password} -e \"CREATE USER 'maxwell'@'%' IDENTIFIED BY 'maxwell';\"" 2>/dev/null || true
  
  sshpass -p "${root_password}" ssh -o StrictHostKeyChecking=no "root@${server}" "mysql -u${mysql_user} -p${mysql_password} -e \"GRANT ALL ON maxwell.* TO 'maxwell'@'%';\""
  
  sshpass -p "${root_password}" ssh -o StrictHostKeyChecking=no "root@${server}" "mysql -u${mysql_user} -p${mysql_password} -e \"GRANT SELECT, REPLICATION CLIENT, REPLICATION SLAVE ON *.* TO 'maxwell'@'%';\""
  
done

#修改Maxwell配置文件名称
for server in "${maxwell_servers[@]}"; do

  sshpass -p "${new_user_password}" ssh -o StrictHostKeyChecking=no "${new_user}@${server}" "cp ${module_path}/${maxwell_version}/config.properties.example ${module_path}/${maxwell_version}/config.properties"
  
done

#修改Maxwell配置文件
for server in "${maxwell_servers[@]}"; do
  
  sshpass -p "${new_user_password}" ssh -o StrictHostKeyChecking=no "${new_user}@${server}" "scp ${new_user}@${master_server}:${autoshell_path}/maxwell_conf/config.properties ${module_path}/${maxwell_version}/"
  
done

echo "maxwell 安装成功"
