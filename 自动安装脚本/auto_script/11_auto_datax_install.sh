#!/bin/bash

source ./common.sh

# 获取版本
datax_version1=$(basename "${datax_package_path}" | awk -F'.tar.gz' '{print $1}')

datax_version="datax"

for server in "${datax_servers[@]}"; do
  # 复制安装包
  sshpass -p "${new_user_password}" ssh -o StrictHostKeyChecking=no "${new_user}@${master_server}" "rsync -av ${datax_package_path}  ${new_user}@${server}:${software_directory}"
done

for server in "${datax_servers[@]}"; do
  # 删除已存在的目录
  sshpass -p "${new_user_password}" ssh -o StrictHostKeyChecking=no "${new_user}@${server}" "sudo rm -rf ${module_path}/datax*"
done

# 解压缩安装包
for server in "${datax_servers[@]}"; do
  
  echo "开始解压 ${server}"
  
  sshpass -p "${new_user_password}" ssh -o StrictHostKeyChecking=no "${new_user}@${server}" "tar -zxf ${datax_package_path} -C ${module_path}/"
done

#自动生成配置
for server in "${datax_servers[@]}"; do
  
  sshpass -p "${new_user_password}" ssh -o StrictHostKeyChecking=no "${new_user}@${server}" "scp ${new_user}@${master_server}:${autoshell_path}/datax_conf/configuration.properties ${module_path}/${datax_version}/"
  
  sshpass -p "${new_user_password}" ssh -o StrictHostKeyChecking=no "${new_user}@${server}" "scp ${new_user}@${master_server}:${autoshell_path}/datax_conf/datax-config-generator-1.0-SNAPSHOT-jar-with-dependencies.jar ${module_path}/${datax_version}/"
  
done

echo "datax 安装成功"
