#!/bin/bash

source ./common.sh

# 获取版本
hive_version1=$(basename "${hive_package_path}" | awk -F'.tar.gz' '{print $1}')

hive_version="hive"

# 配置环境变量
env_variables=(
  "#HIVE_HOME"
  "export HIVE_HOME=${module_path}/${hive_version}"
  "export PATH=\$PATH:\$HIVE_HOME/bin"
)

for server in "${hive_servers[@]}"; do
  # 复制安装包
  sshpass -p "${new_user_password}" ssh -o StrictHostKeyChecking=no "${new_user}@${master_server}" "rsync -av ${hive_package_path}  ${new_user}@${server}:${software_directory}"
done

for server in "${hive_servers[@]}"; do
  # 删除已存在的目录
  sshpass -p "${new_user_password}" ssh -o StrictHostKeyChecking=no "${new_user}@${server}" "sudo rm -rf ${module_path}/hive*"
done

# 解压缩安装包
for server in "${hive_servers[@]}"; do
  
  echo "开始解压 ${server}"
  
  sshpass -p "${new_user_password}" ssh -o StrictHostKeyChecking=no "${new_user}@${server}" "tar -zxf ${hive_package_path} -C ${module_path}/"
done

#修改解压后的文件名称
for server in "${hive_servers[@]}"; do
  sshpass -p "${new_user_password}" ssh -o StrictHostKeyChecking=no "${new_user}@${server}" "mv ${module_path}/apache-hive-3.1.3-bin ${module_path}/${hive_version}"
done

for server in "${hive_servers[@]}"; do
  # 判断my_env.sh文件是否存在，如果不存在，新建
  echo "开始判断my_env.sh文件是否存在"
  sshpass -p "${new_user_password}" ssh -o StrictHostKeyChecking=no "${new_user}@${server}" "sudo touch /etc/profile.d/my_env.sh"
  echo "判断my_env.sh文件是否存在完成"
done

for server in "${hive_servers[@]}"; do
  # 删除原有的配置
  sshpass -p "${new_user_password}" ssh -o StrictHostKeyChecking=no "${new_user}@${server}" "sudo sed -i '/HIVE_HOME/d' /etc/profile.d/my_env.sh"
done

for server in "${hive_servers[@]}"; do
  # 追加新的配置
  for line in "${env_variables[@]}"; do
    sshpass -p "${new_user_password}" ssh -o StrictHostKeyChecking=no "${new_user}@${server}" "echo '${line}' | sudo tee -a /etc/profile.d/my_env.sh"
  done
done

for server in "${hive_servers[@]}"; do
  # 刷新环境变量
  sshpass -p "${new_user_password}" ssh -o StrictHostKeyChecking=no "${new_user}@${server}" "source /etc/profile"
done

#解决日志Jar包冲突，进入/opt/module/hive/lib目录
for server in "${hive_servers[@]}"; do
  sshpass -p "${new_user_password}" ssh -o StrictHostKeyChecking=no "${new_user}@${server}" "mv ${module_path}/${hive_version}/lib/log4j-slf4j-impl-2.17.1.jar ${module_path}/${hive_version}/lib/log4j-slf4j-impl-2.17.1.jar.bak"
done

#将MySQL的JDBC驱动拷贝到Hive的lib目录下
for server in "${hive_servers[@]}"; do

  echo "开始复制配置文件 ${server}"

  sshpass -p "${new_user_password}" ssh -o StrictHostKeyChecking=no "${new_user}@${server}" "scp ${new_user}@${master_server}:${software_directory}mysql/mysql-connector-j-8.0.31.jar ${new_user}@${server}:${module_path}/${hive_version}/lib/"
  
  sshpass -p "${new_user_password}" ssh -o StrictHostKeyChecking=no "${new_user}@${server}" "scp ${new_user}@${master_server}:${autoshell_path}/LiXianShuCang/hive_conf/hive-site.xml ${new_user}@${server}:${module_path}/${hive_version}/conf/"
  
done

#初始化元数据库
for server in "${MySQL_servers[@]}"; do

  sshpass -p "${new_user_password}" ssh -o StrictHostKeyChecking=no "${new_user}@${server}" "mysql -u${mysql_user} -p${mysql_password} -e \"drop database metastore;\"" 2>/dev/null || true

  # 登录到MySQL并创建名为 'metastore' 的数据库
  sshpass -p "${new_user_password}" ssh -o StrictHostKeyChecking=no "${new_user}@${server}" "mysql -u${mysql_user} -p${mysql_password} -e \"create database metastore;\"" 2>/dev/null || true
  
  #初始化Hive元数据库
  sshpass -p "${new_user_password}" ssh -o StrictHostKeyChecking=no "${new_user}@${server}" "schematool -initSchema -dbType mysql -verbose"

  sshpass -p "${new_user_password}" ssh -o StrictHostKeyChecking=no "${new_user}@${server}" "mysql -u${mysql_user} -p${mysql_password} -e \"alter table metastore.COLUMNS_V2 modify column COMMENT varchar(256) character set utf8;\""
  
  sshpass -p "${new_user_password}" ssh -o StrictHostKeyChecking=no "${new_user}@${server}" "mysql -u${mysql_user} -p${mysql_password} -e \"alter table metastore.TABLE_PARAMS modify column PARAM_VALUE mediumtext character set utf8;\""
  
done

#启动hiveserver2
#for server in "${hiveserver2_servers[@]}"; do
#  sshpass -p "${new_user_password}" ssh -o StrictHostKeyChecking=no "${new_user}@${server}" "nohup ${module_path}/${hive_version}/bin/hive --service hiveserver2 2>&1 &"
#done
#
##启动metastore
#for server in "${metastore_servers[@]}"; do
#  sshpass -p "${new_user_password}" ssh -o StrictHostKeyChecking=no "${new_user}@${server}" "nohup ${module_path}/${hive_version}/bin/hive --service metastore 2>&1 &"
#done

echo "hive 安装成功"