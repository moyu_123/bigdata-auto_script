-- 创建表的SQL语句
-- 创建表：tab_name


-- 创建表：dim_activity_full
createtab_stmt
CREATE EXTERNAL TABLE `dim_activity_full`(
  `activity_rule_id` string COMMENT '活动规则ID', 
  `activity_id` string COMMENT '活动ID', 
  `activity_name` string COMMENT '活动名称', 
  `activity_type_code` string COMMENT '活动类型编码', 
  `activity_type_name` string COMMENT '活动类型名称', 
  `activity_desc` string COMMENT '活动描述', 
  `start_time` string COMMENT '开始时间', 
  `end_time` string COMMENT '结束时间', 
  `create_time` string COMMENT '创建时间', 
  `condition_amount` decimal(16,2) COMMENT '满减金额', 
  `condition_num` bigint COMMENT '满减件数', 
  `benefit_amount` decimal(16,2) COMMENT '优惠金额', 
  `benefit_discount` decimal(16,2) COMMENT '优惠折扣', 
  `benefit_rule` string COMMENT '优惠规则', 
  `benefit_level` string COMMENT '优惠级别')
COMMENT '活动维度表'
PARTITIONED BY ( 
  `dt` string)
ROW FORMAT SERDE 
  'org.apache.hadoop.hive.ql.io.orc.OrcSerde' 
STORED AS INPUTFORMAT 
  'org.apache.hadoop.hive.ql.io.orc.OrcInputFormat' 
OUTPUTFORMAT 
  'org.apache.hadoop.hive.ql.io.orc.OrcOutputFormat'
LOCATION
  'hdfs://hadoop102:8020/warehouse/gmall/dim/dim_activity_full'
TBLPROPERTIES (
  'bucketing_version'='2', 
  'orc.compress'='snappy', 
  'transient_lastDdlTime'='1683723096')

-- 创建表：dim_coupon_full
createtab_stmt
CREATE EXTERNAL TABLE `dim_coupon_full`(
  `id` string COMMENT '优惠券编号', 
  `coupon_name` string COMMENT '优惠券名称', 
  `coupon_type_code` string COMMENT '优惠券类型编码', 
  `coupon_type_name` string COMMENT '优惠券类型名称', 
  `condition_amount` decimal(16,2) COMMENT '满额数', 
  `condition_num` bigint COMMENT '满件数', 
  `activity_id` string COMMENT '活动编号', 
  `benefit_amount` decimal(16,2) COMMENT '减免金额', 
  `benefit_discount` decimal(16,2) COMMENT '折扣', 
  `benefit_rule` string COMMENT '优惠规则:满元*减*元，满*件打*折', 
  `create_time` string COMMENT '创建时间', 
  `range_type_code` string COMMENT '优惠范围类型编码', 
  `range_type_name` string COMMENT '优惠范围类型名称', 
  `limit_num` bigint COMMENT '最多领取次数', 
  `taken_count` bigint COMMENT '已领取次数', 
  `start_time` string COMMENT '可以领取的开始时间', 
  `end_time` string COMMENT '可以领取的结束时间', 
  `operate_time` string COMMENT '修改时间', 
  `expire_time` string COMMENT '过期时间')
COMMENT '优惠券维度表'
PARTITIONED BY ( 
  `dt` string)
ROW FORMAT SERDE 
  'org.apache.hadoop.hive.ql.io.orc.OrcSerde' 
STORED AS INPUTFORMAT 
  'org.apache.hadoop.hive.ql.io.orc.OrcInputFormat' 
OUTPUTFORMAT 
  'org.apache.hadoop.hive.ql.io.orc.OrcOutputFormat'
LOCATION
  'hdfs://hadoop102:8020/warehouse/gmall/dim/dim_coupon_full'
TBLPROPERTIES (
  'bucketing_version'='2', 
  'orc.compress'='snappy', 
  'transient_lastDdlTime'='1683721951')

-- 创建表：dim_date
createtab_stmt
CREATE EXTERNAL TABLE `dim_date`(
  `date_id` string COMMENT '日期ID', 
  `week_id` string COMMENT '周ID,一年中的第几周', 
  `week_day` string COMMENT '周几', 
  `day` string COMMENT '每月的第几天', 
  `month` string COMMENT '一年中的第几月', 
  `quarter` string COMMENT '一年中的第几季度', 
  `year` string COMMENT '年份', 
  `is_workday` string COMMENT '是否是工作日', 
  `holiday_id` string COMMENT '节假日')
COMMENT '日期维度表'
ROW FORMAT SERDE 
  'org.apache.hadoop.hive.ql.io.orc.OrcSerde' 
STORED AS INPUTFORMAT 
  'org.apache.hadoop.hive.ql.io.orc.OrcInputFormat' 
OUTPUTFORMAT 
  'org.apache.hadoop.hive.ql.io.orc.OrcOutputFormat'
LOCATION
  'hdfs://hadoop102:8020/warehouse/gmall/dim/dim_date'
TBLPROPERTIES (
  'bucketing_version'='2', 
  'orc.compress'='snappy', 
  'transient_lastDdlTime'='1683980606')

-- 创建表：dim_promotion_pos_full
createtab_stmt
CREATE EXTERNAL TABLE `dim_promotion_pos_full`(
  `id` string COMMENT '营销坑位ID', 
  `pos_location` string COMMENT '营销坑位位置', 
  `pos_type` string COMMENT '营销坑位类型 ', 
  `promotion_type` string COMMENT '营销类型', 
  `create_time` string COMMENT '创建时间', 
  `operate_time` string COMMENT '修改时间')
COMMENT '营销坑位维度表'
PARTITIONED BY ( 
  `dt` string)
ROW FORMAT SERDE 
  'org.apache.hadoop.hive.ql.io.orc.OrcSerde' 
STORED AS INPUTFORMAT 
  'org.apache.hadoop.hive.ql.io.orc.OrcInputFormat' 
OUTPUTFORMAT 
  'org.apache.hadoop.hive.ql.io.orc.OrcOutputFormat'
LOCATION
  'hdfs://hadoop102:8020/warehouse/gmall/dim/dim_promotion_pos_full'
TBLPROPERTIES (
  'bucketing_version'='2', 
  'orc.compress'='snappy', 
  'transient_lastDdlTime'='1683723516')

-- 创建表：dim_promotion_refer_full
createtab_stmt
CREATE EXTERNAL TABLE `dim_promotion_refer_full`(
  `id` string COMMENT '营销渠道ID', 
  `refer_name` string COMMENT '营销渠道名称', 
  `create_time` string COMMENT '创建时间', 
  `operate_time` string COMMENT '修改时间')
COMMENT '营销渠道维度表'
PARTITIONED BY ( 
  `dt` string)
ROW FORMAT SERDE 
  'org.apache.hadoop.hive.ql.io.orc.OrcSerde' 
STORED AS INPUTFORMAT 
  'org.apache.hadoop.hive.ql.io.orc.OrcInputFormat' 
OUTPUTFORMAT 
  'org.apache.hadoop.hive.ql.io.orc.OrcOutputFormat'
LOCATION
  'hdfs://hadoop102:8020/warehouse/gmall/dim/dim_promotion_refer_full'
TBLPROPERTIES (
  'bucketing_version'='2', 
  'orc.compress'='snappy', 
  'transient_lastDdlTime'='1683723767')

-- 创建表：dim_province_full
createtab_stmt
CREATE EXTERNAL TABLE `dim_province_full`(
  `id` string COMMENT '省份ID', 
  `province_name` string COMMENT '省份名称', 
  `area_code` string COMMENT '地区编码', 
  `iso_code` string COMMENT '旧版国际标准地区编码，供可视化使用', 
  `iso_3166_2` string COMMENT '新版国际标准地区编码，供可视化使用', 
  `region_id` string COMMENT '地区ID', 
  `region_name` string COMMENT '地区名称')
COMMENT '地区维度表'
PARTITIONED BY ( 
  `dt` string)
ROW FORMAT SERDE 
  'org.apache.hadoop.hive.ql.io.orc.OrcSerde' 
STORED AS INPUTFORMAT 
  'org.apache.hadoop.hive.ql.io.orc.OrcInputFormat' 
OUTPUTFORMAT 
  'org.apache.hadoop.hive.ql.io.orc.OrcOutputFormat'
LOCATION
  'hdfs://hadoop102:8020/warehouse/gmall/dim/dim_province_full'
TBLPROPERTIES (
  'bucketing_version'='2', 
  'orc.compress'='snappy', 
  'transient_lastDdlTime'='1683723357')

-- 创建表：dim_sku_full
createtab_stmt
CREATE EXTERNAL TABLE `dim_sku_full`(
  `id` string COMMENT 'SKU_ID', 
  `price` decimal(16,2) COMMENT '商品价格', 
  `sku_name` string COMMENT '商品名称', 
  `sku_desc` string COMMENT '商品描述', 
  `weight` decimal(16,2) COMMENT '重量', 
  `is_sale` boolean COMMENT '是否在售', 
  `spu_id` string COMMENT 'SPU编号', 
  `spu_name` string COMMENT 'SPU名称', 
  `category3_id` string COMMENT '三级品类ID', 
  `category3_name` string COMMENT '三级品类名称', 
  `category2_id` string COMMENT '二级品类id', 
  `category2_name` string COMMENT '二级品类名称', 
  `category1_id` string COMMENT '一级品类ID', 
  `category1_name` string COMMENT '一级品类名称', 
  `tm_id` string COMMENT '品牌ID', 
  `tm_name` string COMMENT '品牌名称', 
  `sku_attr_values` array<struct<attr_id:string,value_id:string,attr_name:string,value_name:string>> COMMENT '平台属性', 
  `sku_sale_attr_values` array<struct<sale_attr_id:string,sale_attr_value_id:string,sale_attr_name:string,sale_attr_value_name:string>> COMMENT '销售属性', 
  `create_time` string COMMENT '创建时间')
COMMENT '商品维度表'
PARTITIONED BY ( 
  `dt` string)
ROW FORMAT SERDE 
  'org.apache.hadoop.hive.ql.io.orc.OrcSerde' 
STORED AS INPUTFORMAT 
  'org.apache.hadoop.hive.ql.io.orc.OrcInputFormat' 
OUTPUTFORMAT 
  'org.apache.hadoop.hive.ql.io.orc.OrcOutputFormat'
LOCATION
  'hdfs://hadoop102:8020/warehouse/gmall/dim/dim_sku_full'
TBLPROPERTIES (
  'bucketing_version'='2', 
  'orc.compress'='snappy', 
  'transient_lastDdlTime'='1683720909')

-- 创建表：dim_user_zip
createtab_stmt
CREATE EXTERNAL TABLE `dim_user_zip`(
  `id` string COMMENT '用户ID', 
  `name` string COMMENT '用户姓名', 
  `phone_num` string COMMENT '手机号码', 
  `email` string COMMENT '邮箱', 
  `user_level` string COMMENT '用户等级', 
  `birthday` string COMMENT '生日', 
  `gender` string COMMENT '性别', 
  `create_time` string COMMENT '创建时间', 
  `operate_time` string COMMENT '操作时间', 
  `start_date` string COMMENT '开始日期', 
  `end_date` string COMMENT '结束日期')
COMMENT '用户维度表'
PARTITIONED BY ( 
  `dt` string)
ROW FORMAT SERDE 
  'org.apache.hadoop.hive.ql.io.orc.OrcSerde' 
STORED AS INPUTFORMAT 
  'org.apache.hadoop.hive.ql.io.orc.OrcInputFormat' 
OUTPUTFORMAT 
  'org.apache.hadoop.hive.ql.io.orc.OrcOutputFormat'
LOCATION
  'hdfs://hadoop102:8020/warehouse/gmall/dim/dim_user_zip'
TBLPROPERTIES (
  'bucketing_version'='2', 
  'orc.compress'='snappy', 
  'transient_lastDdlTime'='1683980672')

-- 创建表：dwd_interaction_favor_add_inc
createtab_stmt
CREATE EXTERNAL TABLE `dwd_interaction_favor_add_inc`(
  `id` string COMMENT '编号', 
  `user_id` string COMMENT '用户ID', 
  `sku_id` string COMMENT 'SKU_ID', 
  `date_id` string COMMENT '日期ID', 
  `create_time` string COMMENT '收藏时间')
COMMENT '互动域收藏商品事务事实表'
PARTITIONED BY ( 
  `dt` string)
ROW FORMAT SERDE 
  'org.apache.hadoop.hive.ql.io.orc.OrcSerde' 
STORED AS INPUTFORMAT 
  'org.apache.hadoop.hive.ql.io.orc.OrcInputFormat' 
OUTPUTFORMAT 
  'org.apache.hadoop.hive.ql.io.orc.OrcOutputFormat'
LOCATION
  'hdfs://hadoop102:8020/warehouse/gmall/dwd/dwd_interaction_favor_add_inc'
TBLPROPERTIES (
  'bucketing_version'='2', 
  'orc.compress'='snappy', 
  'transient_lastDdlTime'='1683983765')

-- 创建表：dwd_tool_coupon_used_inc
createtab_stmt
CREATE EXTERNAL TABLE `dwd_tool_coupon_used_inc`(
  `id` string COMMENT '编号', 
  `coupon_id` string COMMENT '优惠券ID', 
  `user_id` string COMMENT '用户ID', 
  `order_id` string COMMENT '订单ID', 
  `date_id` string COMMENT '日期ID', 
  `payment_time` string COMMENT '使用下单时间')
COMMENT '优惠券使用（支付）事务事实表'
PARTITIONED BY ( 
  `dt` string)
ROW FORMAT SERDE 
  'org.apache.hadoop.hive.ql.io.orc.OrcSerde' 
STORED AS INPUTFORMAT 
  'org.apache.hadoop.hive.ql.io.orc.OrcInputFormat' 
OUTPUTFORMAT 
  'org.apache.hadoop.hive.ql.io.orc.OrcOutputFormat'
LOCATION
  'hdfs://hadoop102:8020/warehouse/gmall/dwd/dwd_tool_coupon_used_inc'
TBLPROPERTIES (
  'bucketing_version'='2', 
  'orc.compress'='snappy', 
  'transient_lastDdlTime'='1683983413')

-- 创建表：dwd_trade_cart_add_inc
createtab_stmt
CREATE EXTERNAL TABLE `dwd_trade_cart_add_inc`(
  `id` string COMMENT '编号', 
  `user_id` string COMMENT '用户ID', 
  `sku_id` string COMMENT 'SKU_ID', 
  `date_id` string COMMENT '日期ID', 
  `create_time` string COMMENT '加购时间', 
  `sku_num` bigint COMMENT '加购物车件数')
COMMENT '交易域加购事务事实表'
PARTITIONED BY ( 
  `dt` string)
ROW FORMAT SERDE 
  'org.apache.hadoop.hive.ql.io.orc.OrcSerde' 
STORED AS INPUTFORMAT 
  'org.apache.hadoop.hive.ql.io.orc.OrcInputFormat' 
OUTPUTFORMAT 
  'org.apache.hadoop.hive.ql.io.orc.OrcOutputFormat'
LOCATION
  'hdfs://hadoop102:8020/warehouse/gmall/dwd/dwd_trade_cart_add_inc'
TBLPROPERTIES (
  'bucketing_version'='2', 
  'orc.compress'='snappy', 
  'transient_lastDdlTime'='1683980922')

-- 创建表：dwd_trade_cart_full
createtab_stmt
CREATE EXTERNAL TABLE `dwd_trade_cart_full`(
  `id` string COMMENT '编号', 
  `user_id` string COMMENT '用户ID', 
  `sku_id` string COMMENT 'SKU_ID', 
  `sku_name` string COMMENT '商品名称', 
  `sku_num` bigint COMMENT '现存商品件数')
COMMENT '交易域购物车周期快照事实表'
PARTITIONED BY ( 
  `dt` string)
ROW FORMAT SERDE 
  'org.apache.hadoop.hive.ql.io.orc.OrcSerde' 
STORED AS INPUTFORMAT 
  'org.apache.hadoop.hive.ql.io.orc.OrcInputFormat' 
OUTPUTFORMAT 
  'org.apache.hadoop.hive.ql.io.orc.OrcOutputFormat'
LOCATION
  'hdfs://hadoop102:8020/warehouse/gmall/dwd/dwd_trade_cart_full'
TBLPROPERTIES (
  'bucketing_version'='2', 
  'orc.compress'='snappy', 
  'transient_lastDdlTime'='1683982130')

-- 创建表：dwd_trade_order_detail_inc
createtab_stmt
CREATE EXTERNAL TABLE `dwd_trade_order_detail_inc`(
  `id` string COMMENT '编号', 
  `order_id` string COMMENT '订单ID', 
  `user_id` string COMMENT '用户ID', 
  `sku_id` string COMMENT '商品ID', 
  `province_id` string COMMENT '省份ID', 
  `activity_id` string COMMENT '参与活动ID', 
  `activity_rule_id` string COMMENT '参与活动规则ID', 
  `coupon_id` string COMMENT '使用优惠券ID', 
  `date_id` string COMMENT '下单日期ID', 
  `create_time` string COMMENT '下单时间', 
  `sku_num` bigint COMMENT '商品数量', 
  `split_original_amount` decimal(16,2) COMMENT '原始价格', 
  `split_activity_amount` decimal(16,2) COMMENT '活动优惠分摊', 
  `split_coupon_amount` decimal(16,2) COMMENT '优惠券优惠分摊', 
  `split_total_amount` decimal(16,2) COMMENT '最终价格分摊')
COMMENT '交易域下单事务事实表'
PARTITIONED BY ( 
  `dt` string)
ROW FORMAT SERDE 
  'org.apache.hadoop.hive.ql.io.orc.OrcSerde' 
STORED AS INPUTFORMAT 
  'org.apache.hadoop.hive.ql.io.orc.OrcInputFormat' 
OUTPUTFORMAT 
  'org.apache.hadoop.hive.ql.io.orc.OrcOutputFormat'
LOCATION
  'hdfs://hadoop102:8020/warehouse/gmall/dwd/dwd_trade_order_detail_inc'
TBLPROPERTIES (
  'bucketing_version'='2', 
  'orc.compress'='snappy', 
  'transient_lastDdlTime'='1683981520')

-- 创建表：dwd_trade_pay_detail_suc_inc
createtab_stmt
CREATE EXTERNAL TABLE `dwd_trade_pay_detail_suc_inc`(
  `id` string COMMENT '编号', 
  `order_id` string COMMENT '订单ID', 
  `user_id` string COMMENT '用户ID', 
  `sku_id` string COMMENT 'SKU_ID', 
  `province_id` string COMMENT '省份ID', 
  `activity_id` string COMMENT '参与活动ID', 
  `activity_rule_id` string COMMENT '参与活动规则ID', 
  `coupon_id` string COMMENT '使用优惠券ID', 
  `payment_type_code` string COMMENT '支付类型编码', 
  `payment_type_name` string COMMENT '支付类型名称', 
  `date_id` string COMMENT '支付日期ID', 
  `callback_time` string COMMENT '支付成功时间', 
  `sku_num` bigint COMMENT '商品数量', 
  `split_original_amount` decimal(16,2) COMMENT '应支付原始金额', 
  `split_activity_amount` decimal(16,2) COMMENT '支付活动优惠分摊', 
  `split_coupon_amount` decimal(16,2) COMMENT '支付优惠券优惠分摊', 
  `split_payment_amount` decimal(16,2) COMMENT '支付金额')
COMMENT '交易域支付成功事务事实表'
PARTITIONED BY ( 
  `dt` string)
ROW FORMAT SERDE 
  'org.apache.hadoop.hive.ql.io.orc.OrcSerde' 
STORED AS INPUTFORMAT 
  'org.apache.hadoop.hive.ql.io.orc.OrcInputFormat' 
OUTPUTFORMAT 
  'org.apache.hadoop.hive.ql.io.orc.OrcOutputFormat'
LOCATION
  'hdfs://hadoop102:8020/warehouse/gmall/dwd/dwd_trade_pay_detail_suc_inc'
TBLPROPERTIES (
  'bucketing_version'='2', 
  'orc.compress'='snappy', 
  'transient_lastDdlTime'='1683981746')

-- 创建表：dwd_trade_trade_flow_acc
createtab_stmt
CREATE EXTERNAL TABLE `dwd_trade_trade_flow_acc`(
  `order_id` string COMMENT '订单ID', 
  `user_id` string COMMENT '用户ID', 
  `province_id` string COMMENT '省份ID', 
  `order_date_id` string COMMENT '下单日期ID', 
  `order_time` string COMMENT '下单时间', 
  `payment_date_id` string COMMENT '支付日期ID', 
  `payment_time` string COMMENT '支付时间', 
  `finish_date_id` string COMMENT '确认收货日期ID', 
  `finish_time` string COMMENT '确认收货时间', 
  `order_original_amount` decimal(16,2) COMMENT '下单原始价格', 
  `order_activity_amount` decimal(16,2) COMMENT '下单活动优惠分摊', 
  `order_coupon_amount` decimal(16,2) COMMENT '下单优惠券优惠分摊', 
  `order_total_amount` decimal(16,2) COMMENT '下单最终价格分摊', 
  `payment_amount` decimal(16,2) COMMENT '支付金额')
COMMENT '交易域交易流程累积快照事实表'
PARTITIONED BY ( 
  `dt` string)
ROW FORMAT SERDE 
  'org.apache.hadoop.hive.ql.io.orc.OrcSerde' 
STORED AS INPUTFORMAT 
  'org.apache.hadoop.hive.ql.io.orc.OrcInputFormat' 
OUTPUTFORMAT 
  'org.apache.hadoop.hive.ql.io.orc.OrcOutputFormat'
LOCATION
  'hdfs://hadoop102:8020/warehouse/gmall/dwd/dwd_trade_trade_flow_acc'
TBLPROPERTIES (
  'bucketing_version'='2', 
  'orc.compress'='snappy', 
  'transient_lastDdlTime'='1683982545')

-- 创建表：dwd_traffic_page_view_inc
createtab_stmt
CREATE EXTERNAL TABLE `dwd_traffic_page_view_inc`(
  `province_id` string COMMENT '省份ID', 
  `brand` string COMMENT '手机品牌', 
  `channel` string COMMENT '渠道', 
  `is_new` string COMMENT '是否首次启动', 
  `model` string COMMENT '手机型号', 
  `mid_id` string COMMENT '设备ID', 
  `operate_system` string COMMENT '操作系统', 
  `user_id` string COMMENT '会员ID', 
  `version_code` string COMMENT 'APP版本号', 
  `page_item` string COMMENT '目标ID', 
  `page_item_type` string COMMENT '目标类型', 
  `last_page_id` string COMMENT '上页ID', 
  `page_id` string COMMENT '页面ID ', 
  `from_pos_id` string COMMENT '点击坑位ID', 
  `from_pos_seq` string COMMENT '点击坑位位置', 
  `refer_id` string COMMENT '营销渠道ID', 
  `date_id` string COMMENT '日期ID', 
  `view_time` string COMMENT '跳入时间', 
  `session_id` string COMMENT '所属会话ID', 
  `during_time` bigint COMMENT '持续时间毫秒')
COMMENT '流量域页面浏览事务事实表'
PARTITIONED BY ( 
  `dt` string)
ROW FORMAT SERDE 
  'org.apache.hadoop.hive.ql.io.orc.OrcSerde' 
STORED AS INPUTFORMAT 
  'org.apache.hadoop.hive.ql.io.orc.OrcInputFormat' 
OUTPUTFORMAT 
  'org.apache.hadoop.hive.ql.io.orc.OrcOutputFormat'
LOCATION
  'hdfs://hadoop102:8020/warehouse/gmall/dwd/dwd_traffic_page_view_inc'
TBLPROPERTIES (
  'bucketing_version'='2', 
  'orc.compress'='snappy', 
  'transient_lastDdlTime'='1683984078')

-- 创建表：dwd_user_login_inc
createtab_stmt
CREATE EXTERNAL TABLE `dwd_user_login_inc`(
  `user_id` string COMMENT '用户ID', 
  `date_id` string COMMENT '日期ID', 
  `login_time` string COMMENT '登录时间', 
  `channel` string COMMENT '应用下载渠道', 
  `province_id` string COMMENT '省份ID', 
  `version_code` string COMMENT '应用版本', 
  `mid_id` string COMMENT '设备ID', 
  `brand` string COMMENT '设备品牌', 
  `model` string COMMENT '设备型号', 
  `operate_system` string COMMENT '设备操作系统')
COMMENT '用户域用户登录事务事实表'
PARTITIONED BY ( 
  `dt` string)
ROW FORMAT SERDE 
  'org.apache.hadoop.hive.ql.io.orc.OrcSerde' 
STORED AS INPUTFORMAT 
  'org.apache.hadoop.hive.ql.io.orc.OrcInputFormat' 
OUTPUTFORMAT 
  'org.apache.hadoop.hive.ql.io.orc.OrcOutputFormat'
LOCATION
  'hdfs://hadoop102:8020/warehouse/gmall/dwd/dwd_user_login_inc'
TBLPROPERTIES (
  'bucketing_version'='2', 
  'orc.compress'='snappy', 
  'transient_lastDdlTime'='1683984271')

-- 创建表：dwd_user_register_inc
createtab_stmt
CREATE EXTERNAL TABLE `dwd_user_register_inc`(
  `user_id` string COMMENT '用户ID', 
  `date_id` string COMMENT '日期ID', 
  `create_time` string COMMENT '注册时间', 
  `channel` string COMMENT '应用下载渠道', 
  `province_id` string COMMENT '省份ID', 
  `version_code` string COMMENT '应用版本', 
  `mid_id` string COMMENT '设备ID', 
  `brand` string COMMENT '设备品牌', 
  `model` string COMMENT '设备型号', 
  `operate_system` string COMMENT '设备操作系统')
COMMENT '用户域用户注册事务事实表'
PARTITIONED BY ( 
  `dt` string)
ROW FORMAT SERDE 
  'org.apache.hadoop.hive.ql.io.orc.OrcSerde' 
STORED AS INPUTFORMAT 
  'org.apache.hadoop.hive.ql.io.orc.OrcInputFormat' 
OUTPUTFORMAT 
  'org.apache.hadoop.hive.ql.io.orc.OrcOutputFormat'
LOCATION
  'hdfs://hadoop102:8020/warehouse/gmall/dwd/dwd_user_register_inc'
TBLPROPERTIES (
  'bucketing_version'='2', 
  'orc.compress'='snappy', 
  'transient_lastDdlTime'='1683984191')

-- 创建表：ods_activity_info_full
createtab_stmt
CREATE EXTERNAL TABLE `ods_activity_info_full`(
  `id` string COMMENT '活动id', 
  `activity_name` string COMMENT '活动名称', 
  `activity_type` string COMMENT '活动类型', 
  `activity_desc` string COMMENT '活动描述', 
  `start_time` string COMMENT '开始时间', 
  `end_time` string COMMENT '结束时间', 
  `create_time` string COMMENT '创建时间', 
  `operate_time` string COMMENT '修改时间')
COMMENT '活动信息表'
PARTITIONED BY ( 
  `dt` string)
ROW FORMAT SERDE 
  'org.apache.hadoop.hive.serde2.lazy.LazySimpleSerDe' 
WITH SERDEPROPERTIES ( 
  'field.delim'='\t', 
  'serialization.format'='\t', 
  'serialization.null.format'='') 
STORED AS INPUTFORMAT 
  'org.apache.hadoop.mapred.TextInputFormat' 
OUTPUTFORMAT 
  'org.apache.hadoop.hive.ql.io.HiveIgnoreKeyTextOutputFormat'
LOCATION
  'hdfs://hadoop102:8020/warehouse/gmall/ods/ods_activity_info_full'
TBLPROPERTIES (
  'bucketing_version'='2', 
  'compression.codec'='org.apache.hadoop.io.compress.GzipCodec', 
  'transient_lastDdlTime'='1683720611')

-- 创建表：ods_activity_rule_full
createtab_stmt
CREATE EXTERNAL TABLE `ods_activity_rule_full`(
  `id` string COMMENT '编号', 
  `activity_id` string COMMENT '活动ID', 
  `activity_type` string COMMENT '活动类型', 
  `condition_amount` decimal(16,2) COMMENT '满减金额', 
  `condition_num` bigint COMMENT '满减件数', 
  `benefit_amount` decimal(16,2) COMMENT '优惠金额', 
  `benefit_discount` decimal(16,2) COMMENT '优惠折扣', 
  `benefit_level` string COMMENT '优惠级别', 
  `create_time` string COMMENT '创建时间', 
  `operate_time` string COMMENT '修改时间')
COMMENT '活动规则表'
PARTITIONED BY ( 
  `dt` string)
ROW FORMAT SERDE 
  'org.apache.hadoop.hive.serde2.lazy.LazySimpleSerDe' 
WITH SERDEPROPERTIES ( 
  'field.delim'='\t', 
  'serialization.format'='\t', 
  'serialization.null.format'='') 
STORED AS INPUTFORMAT 
  'org.apache.hadoop.mapred.TextInputFormat' 
OUTPUTFORMAT 
  'org.apache.hadoop.hive.ql.io.HiveIgnoreKeyTextOutputFormat'
LOCATION
  'hdfs://hadoop102:8020/warehouse/gmall/ods/ods_activity_rule_full'
TBLPROPERTIES (
  'bucketing_version'='2', 
  'compression.codec'='org.apache.hadoop.io.compress.GzipCodec', 
  'transient_lastDdlTime'='1683720611')

-- 创建表：ods_base_category1_full
createtab_stmt
CREATE EXTERNAL TABLE `ods_base_category1_full`(
  `id` string COMMENT '编号', 
  `name` string COMMENT '分类名称', 
  `create_time` string COMMENT '创建时间', 
  `operate_time` string COMMENT '修改时间')
COMMENT '一级品类表'
PARTITIONED BY ( 
  `dt` string)
ROW FORMAT SERDE 
  'org.apache.hadoop.hive.serde2.lazy.LazySimpleSerDe' 
WITH SERDEPROPERTIES ( 
  'field.delim'='\t', 
  'serialization.format'='\t', 
  'serialization.null.format'='') 
STORED AS INPUTFORMAT 
  'org.apache.hadoop.mapred.TextInputFormat' 
OUTPUTFORMAT 
  'org.apache.hadoop.hive.ql.io.HiveIgnoreKeyTextOutputFormat'
LOCATION
  'hdfs://hadoop102:8020/warehouse/gmall/ods/ods_base_category1_full'
TBLPROPERTIES (
  'bucketing_version'='2', 
  'compression.codec'='org.apache.hadoop.io.compress.GzipCodec', 
  'transient_lastDdlTime'='1683720611')

-- 创建表：ods_base_category2_full
createtab_stmt
CREATE EXTERNAL TABLE `ods_base_category2_full`(
  `id` string COMMENT '编号', 
  `name` string COMMENT '二级分类名称', 
  `category1_id` string COMMENT '一级分类编号', 
  `create_time` string COMMENT '创建时间', 
  `operate_time` string COMMENT '修改时间')
COMMENT '二级品类表'
PARTITIONED BY ( 
  `dt` string)
ROW FORMAT SERDE 
  'org.apache.hadoop.hive.serde2.lazy.LazySimpleSerDe' 
WITH SERDEPROPERTIES ( 
  'field.delim'='\t', 
  'serialization.format'='\t', 
  'serialization.null.format'='') 
STORED AS INPUTFORMAT 
  'org.apache.hadoop.mapred.TextInputFormat' 
OUTPUTFORMAT 
  'org.apache.hadoop.hive.ql.io.HiveIgnoreKeyTextOutputFormat'
LOCATION
  'hdfs://hadoop102:8020/warehouse/gmall/ods/ods_base_category2_full'
TBLPROPERTIES (
  'bucketing_version'='2', 
  'compression.codec'='org.apache.hadoop.io.compress.GzipCodec', 
  'transient_lastDdlTime'='1683720612')

-- 创建表：ods_base_category3_full
createtab_stmt
CREATE EXTERNAL TABLE `ods_base_category3_full`(
  `id` string COMMENT '编号', 
  `name` string COMMENT '三级分类名称', 
  `category2_id` string COMMENT '二级分类编号', 
  `create_time` string COMMENT '创建时间', 
  `operate_time` string COMMENT '修改时间')
COMMENT '三级品类表'
PARTITIONED BY ( 
  `dt` string)
ROW FORMAT SERDE 
  'org.apache.hadoop.hive.serde2.lazy.LazySimpleSerDe' 
WITH SERDEPROPERTIES ( 
  'field.delim'='\t', 
  'serialization.format'='\t', 
  'serialization.null.format'='') 
STORED AS INPUTFORMAT 
  'org.apache.hadoop.mapred.TextInputFormat' 
OUTPUTFORMAT 
  'org.apache.hadoop.hive.ql.io.HiveIgnoreKeyTextOutputFormat'
LOCATION
  'hdfs://hadoop102:8020/warehouse/gmall/ods/ods_base_category3_full'
TBLPROPERTIES (
  'bucketing_version'='2', 
  'compression.codec'='org.apache.hadoop.io.compress.GzipCodec', 
  'transient_lastDdlTime'='1683720612')

-- 创建表：ods_base_dic_full
createtab_stmt
CREATE EXTERNAL TABLE `ods_base_dic_full`(
  `dic_code` string COMMENT '编号', 
  `dic_name` string COMMENT '编码名称', 
  `parent_code` string COMMENT '父编号', 
  `create_time` string COMMENT '创建日期', 
  `operate_time` string COMMENT '修改日期')
COMMENT '编码字典表'
PARTITIONED BY ( 
  `dt` string)
ROW FORMAT SERDE 
  'org.apache.hadoop.hive.serde2.lazy.LazySimpleSerDe' 
WITH SERDEPROPERTIES ( 
  'field.delim'='\t', 
  'serialization.format'='\t', 
  'serialization.null.format'='') 
STORED AS INPUTFORMAT 
  'org.apache.hadoop.mapred.TextInputFormat' 
OUTPUTFORMAT 
  'org.apache.hadoop.hive.ql.io.HiveIgnoreKeyTextOutputFormat'
LOCATION
  'hdfs://hadoop102:8020/warehouse/gmall/ods/ods_base_dic_full'
TBLPROPERTIES (
  'bucketing_version'='2', 
  'compression.codec'='org.apache.hadoop.io.compress.GzipCodec', 
  'transient_lastDdlTime'='1683720612')

-- 创建表：ods_base_province_full
createtab_stmt
CREATE EXTERNAL TABLE `ods_base_province_full`(
  `id` string COMMENT '编号', 
  `name` string COMMENT '省份名称', 
  `region_id` string COMMENT '地区ID', 
  `area_code` string COMMENT '地区编码', 
  `iso_code` string COMMENT '旧版国际标准地区编码，供可视化使用', 
  `iso_3166_2` string COMMENT '新版国际标准地区编码，供可视化使用', 
  `create_time` string COMMENT '创建时间', 
  `operate_time` string COMMENT '修改时间')
COMMENT '省份表'
PARTITIONED BY ( 
  `dt` string)
ROW FORMAT SERDE 
  'org.apache.hadoop.hive.serde2.lazy.LazySimpleSerDe' 
WITH SERDEPROPERTIES ( 
  'field.delim'='\t', 
  'serialization.format'='\t', 
  'serialization.null.format'='') 
STORED AS INPUTFORMAT 
  'org.apache.hadoop.mapred.TextInputFormat' 
OUTPUTFORMAT 
  'org.apache.hadoop.hive.ql.io.HiveIgnoreKeyTextOutputFormat'
LOCATION
  'hdfs://hadoop102:8020/warehouse/gmall/ods/ods_base_province_full'
TBLPROPERTIES (
  'bucketing_version'='2', 
  'compression.codec'='org.apache.hadoop.io.compress.GzipCodec', 
  'transient_lastDdlTime'='1683720612')

-- 创建表：ods_base_region_full
createtab_stmt
CREATE EXTERNAL TABLE `ods_base_region_full`(
  `id` string COMMENT '地区ID', 
  `region_name` string COMMENT '地区名称', 
  `create_time` string COMMENT '创建时间', 
  `operate_time` string COMMENT '修改时间')
COMMENT '地区表'
PARTITIONED BY ( 
  `dt` string)
ROW FORMAT SERDE 
  'org.apache.hadoop.hive.serde2.lazy.LazySimpleSerDe' 
WITH SERDEPROPERTIES ( 
  'field.delim'='\t', 
  'serialization.format'='\t', 
  'serialization.null.format'='') 
STORED AS INPUTFORMAT 
  'org.apache.hadoop.mapred.TextInputFormat' 
OUTPUTFORMAT 
  'org.apache.hadoop.hive.ql.io.HiveIgnoreKeyTextOutputFormat'
LOCATION
  'hdfs://hadoop102:8020/warehouse/gmall/ods/ods_base_region_full'
TBLPROPERTIES (
  'bucketing_version'='2', 
  'compression.codec'='org.apache.hadoop.io.compress.GzipCodec', 
  'transient_lastDdlTime'='1683720613')

-- 创建表：ods_base_trademark_full
createtab_stmt
CREATE EXTERNAL TABLE `ods_base_trademark_full`(
  `id` string COMMENT '编号', 
  `tm_name` string COMMENT '品牌名称', 
  `logo_url` string COMMENT '品牌LOGO的图片路径', 
  `create_time` string COMMENT '创建时间', 
  `operate_time` string COMMENT '修改时间')
COMMENT '品牌表'
PARTITIONED BY ( 
  `dt` string)
ROW FORMAT SERDE 
  'org.apache.hadoop.hive.serde2.lazy.LazySimpleSerDe' 
WITH SERDEPROPERTIES ( 
  'field.delim'='\t', 
  'serialization.format'='\t', 
  'serialization.null.format'='') 
STORED AS INPUTFORMAT 
  'org.apache.hadoop.mapred.TextInputFormat' 
OUTPUTFORMAT 
  'org.apache.hadoop.hive.ql.io.HiveIgnoreKeyTextOutputFormat'
LOCATION
  'hdfs://hadoop102:8020/warehouse/gmall/ods/ods_base_trademark_full'
TBLPROPERTIES (
  'bucketing_version'='2', 
  'compression.codec'='org.apache.hadoop.io.compress.GzipCodec', 
  'transient_lastDdlTime'='1683720613')

-- 创建表：ods_cart_info_full
createtab_stmt
CREATE EXTERNAL TABLE `ods_cart_info_full`(
  `id` string COMMENT '编号', 
  `user_id` string COMMENT '用户ID', 
  `sku_id` string COMMENT 'SKU_ID', 
  `cart_price` decimal(16,2) COMMENT '放入购物车时价格', 
  `sku_num` bigint COMMENT '数量', 
  `img_url` bigint COMMENT '商品图片地址', 
  `sku_name` string COMMENT 'SKU名称 (冗余)', 
  `is_checked` string COMMENT '是否被选中', 
  `create_time` string COMMENT '创建时间', 
  `operate_time` string COMMENT '修改时间', 
  `is_ordered` string COMMENT '是否已经下单', 
  `order_time` string COMMENT '下单时间')
COMMENT '购物车全量表'
PARTITIONED BY ( 
  `dt` string)
ROW FORMAT SERDE 
  'org.apache.hadoop.hive.serde2.lazy.LazySimpleSerDe' 
WITH SERDEPROPERTIES ( 
  'field.delim'='\t', 
  'serialization.format'='\t', 
  'serialization.null.format'='') 
STORED AS INPUTFORMAT 
  'org.apache.hadoop.mapred.TextInputFormat' 
OUTPUTFORMAT 
  'org.apache.hadoop.hive.ql.io.HiveIgnoreKeyTextOutputFormat'
LOCATION
  'hdfs://hadoop102:8020/warehouse/gmall/ods/ods_cart_info_full'
TBLPROPERTIES (
  'bucketing_version'='2', 
  'compression.codec'='org.apache.hadoop.io.compress.GzipCodec', 
  'transient_lastDdlTime'='1683720613')

-- 创建表：ods_cart_info_inc
createtab_stmt
CREATE EXTERNAL TABLE `ods_cart_info_inc`(
  `type` string COMMENT 'from deserializer', 
  `ts` bigint COMMENT 'from deserializer', 
  `data` struct<id:string,user_id:string,sku_id:string,cart_price:decimal(16,2),sku_num:bigint,img_url:string,sku_name:string,is_checked:string,create_time:string,operate_time:string,is_ordered:string,order_time:string> COMMENT 'from deserializer', 
  `old` map<string,string> COMMENT 'from deserializer')
COMMENT '购物车增量表'
PARTITIONED BY ( 
  `dt` string)
ROW FORMAT SERDE 
  'org.apache.hadoop.hive.serde2.JsonSerDe' 
STORED AS INPUTFORMAT 
  'org.apache.hadoop.mapred.TextInputFormat' 
OUTPUTFORMAT 
  'org.apache.hadoop.hive.ql.io.HiveIgnoreKeyTextOutputFormat'
LOCATION
  'hdfs://hadoop102:8020/warehouse/gmall/ods/ods_cart_info_inc'
TBLPROPERTIES (
  'bucketing_version'='2', 
  'compression.codec'='org.apache.hadoop.io.compress.GzipCodec', 
  'transient_lastDdlTime'='1683720615')

-- 创建表：ods_comment_info_inc
createtab_stmt
CREATE EXTERNAL TABLE `ods_comment_info_inc`(
  `type` string COMMENT 'from deserializer', 
  `ts` bigint COMMENT 'from deserializer', 
  `data` struct<id:string,user_id:string,nick_name:string,head_img:string,sku_id:string,spu_id:string,order_id:string,appraise:string,comment_txt:string,create_time:string,operate_time:string> COMMENT 'from deserializer', 
  `old` map<string,string> COMMENT 'from deserializer')
COMMENT '评论表'
PARTITIONED BY ( 
  `dt` string)
ROW FORMAT SERDE 
  'org.apache.hadoop.hive.serde2.JsonSerDe' 
STORED AS INPUTFORMAT 
  'org.apache.hadoop.mapred.TextInputFormat' 
OUTPUTFORMAT 
  'org.apache.hadoop.hive.ql.io.HiveIgnoreKeyTextOutputFormat'
LOCATION
  'hdfs://hadoop102:8020/warehouse/gmall/ods/ods_comment_info_inc'
TBLPROPERTIES (
  'bucketing_version'='2', 
  'compression.codec'='org.apache.hadoop.io.compress.GzipCodec', 
  'transient_lastDdlTime'='1683720615')

-- 创建表：ods_coupon_info_full
createtab_stmt
CREATE EXTERNAL TABLE `ods_coupon_info_full`(
  `id` string COMMENT '购物券编号', 
  `coupon_name` string COMMENT '购物券名称', 
  `coupon_type` string COMMENT '购物券类型 1 现金券 2 折扣券 3 满减券 4 满件打折券', 
  `condition_amount` decimal(16,2) COMMENT '满额数', 
  `condition_num` bigint COMMENT '满件数', 
  `activity_id` string COMMENT '活动编号', 
  `benefit_amount` decimal(16,2) COMMENT '减免金额', 
  `benefit_discount` decimal(16,2) COMMENT '折扣', 
  `create_time` string COMMENT '创建时间', 
  `range_type` string COMMENT '范围类型 1、商品(SPUID) 2、品类(三级品类id) 3、品牌', 
  `limit_num` bigint COMMENT '最多领用次数', 
  `taken_count` bigint COMMENT '已领用次数', 
  `start_time` string COMMENT '可以领取的开始时间', 
  `end_time` string COMMENT '可以领取的结束时间', 
  `operate_time` string COMMENT '修改时间', 
  `expire_time` string COMMENT '过期时间', 
  `range_desc` string COMMENT '范围描述')
COMMENT '优惠券信息表'
PARTITIONED BY ( 
  `dt` string)
ROW FORMAT SERDE 
  'org.apache.hadoop.hive.serde2.lazy.LazySimpleSerDe' 
WITH SERDEPROPERTIES ( 
  'field.delim'='\t', 
  'serialization.format'='\t', 
  'serialization.null.format'='') 
STORED AS INPUTFORMAT 
  'org.apache.hadoop.mapred.TextInputFormat' 
OUTPUTFORMAT 
  'org.apache.hadoop.hive.ql.io.HiveIgnoreKeyTextOutputFormat'
LOCATION
  'hdfs://hadoop102:8020/warehouse/gmall/ods/ods_coupon_info_full'
TBLPROPERTIES (
  'bucketing_version'='2', 
  'compression.codec'='org.apache.hadoop.io.compress.GzipCodec', 
  'transient_lastDdlTime'='1683720613')

-- 创建表：ods_coupon_use_inc
createtab_stmt
CREATE EXTERNAL TABLE `ods_coupon_use_inc`(
  `type` string COMMENT 'from deserializer', 
  `ts` bigint COMMENT 'from deserializer', 
  `data` struct<id:string,coupon_id:string,user_id:string,order_id:string,coupon_status:string,get_time:string,using_time:string,used_time:string,expire_time:string,create_time:string,operate_time:string> COMMENT 'from deserializer', 
  `old` map<string,string> COMMENT 'from deserializer')
COMMENT '优惠券领用表'
PARTITIONED BY ( 
  `dt` string)
ROW FORMAT SERDE 
  'org.apache.hadoop.hive.serde2.JsonSerDe' 
STORED AS INPUTFORMAT 
  'org.apache.hadoop.mapred.TextInputFormat' 
OUTPUTFORMAT 
  'org.apache.hadoop.hive.ql.io.HiveIgnoreKeyTextOutputFormat'
LOCATION
  'hdfs://hadoop102:8020/warehouse/gmall/ods/ods_coupon_use_inc'
TBLPROPERTIES (
  'bucketing_version'='2', 
  'compression.codec'='org.apache.hadoop.io.compress.GzipCodec', 
  'transient_lastDdlTime'='1683720616')

-- 创建表：ods_favor_info_inc
createtab_stmt
CREATE EXTERNAL TABLE `ods_favor_info_inc`(
  `type` string COMMENT 'from deserializer', 
  `ts` bigint COMMENT 'from deserializer', 
  `data` struct<id:string,user_id:string,sku_id:string,spu_id:string,is_cancel:string,create_time:string,operate_time:string> COMMENT 'from deserializer', 
  `old` map<string,string> COMMENT 'from deserializer')
COMMENT '收藏表'
PARTITIONED BY ( 
  `dt` string)
ROW FORMAT SERDE 
  'org.apache.hadoop.hive.serde2.JsonSerDe' 
STORED AS INPUTFORMAT 
  'org.apache.hadoop.mapred.TextInputFormat' 
OUTPUTFORMAT 
  'org.apache.hadoop.hive.ql.io.HiveIgnoreKeyTextOutputFormat'
LOCATION
  'hdfs://hadoop102:8020/warehouse/gmall/ods/ods_favor_info_inc'
TBLPROPERTIES (
  'bucketing_version'='2', 
  'compression.codec'='org.apache.hadoop.io.compress.GzipCodec', 
  'transient_lastDdlTime'='1683720616')

-- 创建表：ods_log_inc
createtab_stmt
CREATE EXTERNAL TABLE `ods_log_inc`(
  `common` struct<ar:string,ba:string,ch:string,is_new:string,md:string,mid:string,os:string,sid:string,uid:string,vc:string> COMMENT 'from deserializer', 
  `page` struct<during_time:string,item:string,item_type:string,last_page_id:string,page_id:string,from_pos_id:string,from_pos_seq:string,refer_id:string> COMMENT 'from deserializer', 
  `actions` array<struct<action_id:string,item:string,item_type:string,ts:bigint>> COMMENT 'from deserializer', 
  `displays` array<struct<display_type:string,item:string,item_type:string,pos_seq:string,pos_id:string>> COMMENT 'from deserializer', 
  `start` struct<entry:string,first_open:bigint,loading_time:bigint,open_ad_id:bigint,open_ad_ms:bigint,open_ad_skip_ms:bigint> COMMENT 'from deserializer', 
  `err` struct<error_code:bigint,msg:string> COMMENT 'from deserializer', 
  `ts` bigint COMMENT 'from deserializer')
COMMENT '活动信息表'
PARTITIONED BY ( 
  `dt` string)
ROW FORMAT SERDE 
  'org.apache.hadoop.hive.serde2.JsonSerDe' 
STORED AS INPUTFORMAT 
  'org.apache.hadoop.mapred.TextInputFormat' 
OUTPUTFORMAT 
  'org.apache.hadoop.hive.ql.io.HiveIgnoreKeyTextOutputFormat'
LOCATION
  'hdfs://hadoop102:8020/warehouse/gmall/ods/ods_log_inc'
TBLPROPERTIES (
  'bucketing_version'='2', 
  'compression.codec'='org.apache.hadoop.io.compress.GzipCodec', 
  'transient_lastDdlTime'='1683719602')

-- 创建表：ods_order_detail_activity_inc
createtab_stmt
CREATE EXTERNAL TABLE `ods_order_detail_activity_inc`(
  `type` string COMMENT 'from deserializer', 
  `ts` bigint COMMENT 'from deserializer', 
  `data` struct<id:string,order_id:string,order_detail_id:string,activity_id:string,activity_rule_id:string,sku_id:string,create_time:string,operate_time:string> COMMENT 'from deserializer', 
  `old` map<string,string> COMMENT 'from deserializer')
COMMENT '订单明细活动关联表'
PARTITIONED BY ( 
  `dt` string)
ROW FORMAT SERDE 
  'org.apache.hadoop.hive.serde2.JsonSerDe' 
STORED AS INPUTFORMAT 
  'org.apache.hadoop.mapred.TextInputFormat' 
OUTPUTFORMAT 
  'org.apache.hadoop.hive.ql.io.HiveIgnoreKeyTextOutputFormat'
LOCATION
  'hdfs://hadoop102:8020/warehouse/gmall/ods/ods_order_detail_activity_inc'
TBLPROPERTIES (
  'bucketing_version'='2', 
  'compression.codec'='org.apache.hadoop.io.compress.GzipCodec', 
  'transient_lastDdlTime'='1683720616')

-- 创建表：ods_order_detail_coupon_inc
createtab_stmt
CREATE EXTERNAL TABLE `ods_order_detail_coupon_inc`(
  `type` string COMMENT 'from deserializer', 
  `ts` bigint COMMENT 'from deserializer', 
  `data` struct<id:string,order_id:string,order_detail_id:string,coupon_id:string,coupon_use_id:string,sku_id:string,create_time:string,operate_time:string> COMMENT 'from deserializer', 
  `old` map<string,string> COMMENT 'from deserializer')
COMMENT '订单明细优惠券关联表'
PARTITIONED BY ( 
  `dt` string)
ROW FORMAT SERDE 
  'org.apache.hadoop.hive.serde2.JsonSerDe' 
STORED AS INPUTFORMAT 
  'org.apache.hadoop.mapred.TextInputFormat' 
OUTPUTFORMAT 
  'org.apache.hadoop.hive.ql.io.HiveIgnoreKeyTextOutputFormat'
LOCATION
  'hdfs://hadoop102:8020/warehouse/gmall/ods/ods_order_detail_coupon_inc'
TBLPROPERTIES (
  'bucketing_version'='2', 
  'compression.codec'='org.apache.hadoop.io.compress.GzipCodec', 
  'transient_lastDdlTime'='1683720616')

-- 创建表：ods_order_detail_inc
createtab_stmt
CREATE EXTERNAL TABLE `ods_order_detail_inc`(
  `type` string COMMENT 'from deserializer', 
  `ts` bigint COMMENT 'from deserializer', 
  `data` struct<id:string,order_id:string,sku_id:string,sku_name:string,img_url:string,order_price:decimal(16,2),sku_num:bigint,create_time:string,source_type:string,source_id:string,split_total_amount:decimal(16,2),split_activity_amount:decimal(16,2),split_coupon_amount:decimal(16,2),operate_time:string> COMMENT 'from deserializer', 
  `old` map<string,string> COMMENT 'from deserializer')
COMMENT '订单明细表'
PARTITIONED BY ( 
  `dt` string)
ROW FORMAT SERDE 
  'org.apache.hadoop.hive.serde2.JsonSerDe' 
STORED AS INPUTFORMAT 
  'org.apache.hadoop.mapred.TextInputFormat' 
OUTPUTFORMAT 
  'org.apache.hadoop.hive.ql.io.HiveIgnoreKeyTextOutputFormat'
LOCATION
  'hdfs://hadoop102:8020/warehouse/gmall/ods/ods_order_detail_inc'
TBLPROPERTIES (
  'bucketing_version'='2', 
  'compression.codec'='org.apache.hadoop.io.compress.GzipCodec', 
  'transient_lastDdlTime'='1683720616')

-- 创建表：ods_order_info_inc
createtab_stmt
CREATE EXTERNAL TABLE `ods_order_info_inc`(
  `type` string COMMENT 'from deserializer', 
  `ts` bigint COMMENT 'from deserializer', 
  `data` struct<id:string,consignee:string,consignee_tel:string,total_amount:decimal(16,2),order_status:string,user_id:string,payment_way:string,delivery_address:string,order_comment:string,out_trade_no:string,trade_body:string,create_time:string,operate_time:string,expire_time:string,process_status:string,tracking_no:string,parent_order_id:string,img_url:string,province_id:string,activity_reduce_amount:decimal(16,2),coupon_reduce_amount:decimal(16,2),original_total_amount:decimal(16,2),freight_fee:decimal(16,2),freight_fee_reduce:decimal(16,2),refundable_time:decimal(16,2)> COMMENT 'from deserializer', 
  `old` map<string,string> COMMENT 'from deserializer')
COMMENT '订单表'
PARTITIONED BY ( 
  `dt` string)
ROW FORMAT SERDE 
  'org.apache.hadoop.hive.serde2.JsonSerDe' 
STORED AS INPUTFORMAT 
  'org.apache.hadoop.mapred.TextInputFormat' 
OUTPUTFORMAT 
  'org.apache.hadoop.hive.ql.io.HiveIgnoreKeyTextOutputFormat'
LOCATION
  'hdfs://hadoop102:8020/warehouse/gmall/ods/ods_order_info_inc'
TBLPROPERTIES (
  'bucketing_version'='2', 
  'compression.codec'='org.apache.hadoop.io.compress.GzipCodec', 
  'transient_lastDdlTime'='1683720617')

-- 创建表：ods_order_refund_info_inc
createtab_stmt
CREATE EXTERNAL TABLE `ods_order_refund_info_inc`(
  `type` string COMMENT 'from deserializer', 
  `ts` bigint COMMENT 'from deserializer', 
  `data` struct<id:string,user_id:string,order_id:string,sku_id:string,refund_type:string,refund_num:bigint,refund_amount:decimal(16,2),refund_reason_type:string,refund_reason_txt:string,refund_status:string,create_time:string,operate_time:string> COMMENT 'from deserializer', 
  `old` map<string,string> COMMENT 'from deserializer')
COMMENT '退单表'
PARTITIONED BY ( 
  `dt` string)
ROW FORMAT SERDE 
  'org.apache.hadoop.hive.serde2.JsonSerDe' 
STORED AS INPUTFORMAT 
  'org.apache.hadoop.mapred.TextInputFormat' 
OUTPUTFORMAT 
  'org.apache.hadoop.hive.ql.io.HiveIgnoreKeyTextOutputFormat'
LOCATION
  'hdfs://hadoop102:8020/warehouse/gmall/ods/ods_order_refund_info_inc'
TBLPROPERTIES (
  'bucketing_version'='2', 
  'compression.codec'='org.apache.hadoop.io.compress.GzipCodec', 
  'transient_lastDdlTime'='1683720617')

-- 创建表：ods_order_status_log_inc
createtab_stmt
CREATE EXTERNAL TABLE `ods_order_status_log_inc`(
  `type` string COMMENT 'from deserializer', 
  `ts` bigint COMMENT 'from deserializer', 
  `data` struct<id:string,order_id:string,order_status:string,create_time:string,operate_time:string> COMMENT 'from deserializer', 
  `old` map<string,string> COMMENT 'from deserializer')
COMMENT '订单状态流水表'
PARTITIONED BY ( 
  `dt` string)
ROW FORMAT SERDE 
  'org.apache.hadoop.hive.serde2.JsonSerDe' 
STORED AS INPUTFORMAT 
  'org.apache.hadoop.mapred.TextInputFormat' 
OUTPUTFORMAT 
  'org.apache.hadoop.hive.ql.io.HiveIgnoreKeyTextOutputFormat'
LOCATION
  'hdfs://hadoop102:8020/warehouse/gmall/ods/ods_order_status_log_inc'
TBLPROPERTIES (
  'bucketing_version'='2', 
  'compression.codec'='org.apache.hadoop.io.compress.GzipCodec', 
  'transient_lastDdlTime'='1683720617')

-- 创建表：ods_payment_info_inc
createtab_stmt
CREATE EXTERNAL TABLE `ods_payment_info_inc`(
  `type` string COMMENT 'from deserializer', 
  `ts` bigint COMMENT 'from deserializer', 
  `data` struct<id:string,out_trade_no:string,order_id:string,user_id:string,payment_type:string,trade_no:string,total_amount:decimal(16,2),subject:string,payment_status:string,create_time:string,callback_time:string,callback_content:string,operate_time:string> COMMENT 'from deserializer', 
  `old` map<string,string> COMMENT 'from deserializer')
COMMENT '支付表'
PARTITIONED BY ( 
  `dt` string)
ROW FORMAT SERDE 
  'org.apache.hadoop.hive.serde2.JsonSerDe' 
STORED AS INPUTFORMAT 
  'org.apache.hadoop.mapred.TextInputFormat' 
OUTPUTFORMAT 
  'org.apache.hadoop.hive.ql.io.HiveIgnoreKeyTextOutputFormat'
LOCATION
  'hdfs://hadoop102:8020/warehouse/gmall/ods/ods_payment_info_inc'
TBLPROPERTIES (
  'bucketing_version'='2', 
  'compression.codec'='org.apache.hadoop.io.compress.GzipCodec', 
  'transient_lastDdlTime'='1683720617')

-- 创建表：ods_promotion_pos_full
createtab_stmt
CREATE EXTERNAL TABLE `ods_promotion_pos_full`(
  `id` string COMMENT '营销坑位ID', 
  `pos_location` string COMMENT '营销坑位位置', 
  `pos_type` string COMMENT '营销坑位类型：banner,宫格,列表,瀑布', 
  `promotion_type` string COMMENT '营销类型：算法、固定、搜索', 
  `create_time` string COMMENT '创建时间', 
  `operate_time` string COMMENT '修改时间')
COMMENT '营销坑位表'
PARTITIONED BY ( 
  `dt` string)
ROW FORMAT SERDE 
  'org.apache.hadoop.hive.serde2.lazy.LazySimpleSerDe' 
WITH SERDEPROPERTIES ( 
  'field.delim'='\t', 
  'serialization.format'='\t', 
  'serialization.null.format'='') 
STORED AS INPUTFORMAT 
  'org.apache.hadoop.mapred.TextInputFormat' 
OUTPUTFORMAT 
  'org.apache.hadoop.hive.ql.io.HiveIgnoreKeyTextOutputFormat'
LOCATION
  'hdfs://hadoop102:8020/warehouse/gmall/ods/ods_promotion_pos_full'
TBLPROPERTIES (
  'bucketing_version'='2', 
  'compression.codec'='org.apache.hadoop.io.compress.GzipCodec', 
  'transient_lastDdlTime'='1683720615')

-- 创建表：ods_promotion_refer_full
createtab_stmt
CREATE EXTERNAL TABLE `ods_promotion_refer_full`(
  `id` string COMMENT '外部营销渠道ID', 
  `refer_name` string COMMENT '外部营销渠道名称', 
  `create_time` string COMMENT '创建时间', 
  `operate_time` string COMMENT '修改时间')
COMMENT '营销渠道表'
PARTITIONED BY ( 
  `dt` string)
ROW FORMAT SERDE 
  'org.apache.hadoop.hive.serde2.lazy.LazySimpleSerDe' 
WITH SERDEPROPERTIES ( 
  'field.delim'='\t', 
  'serialization.format'='\t', 
  'serialization.null.format'='') 
STORED AS INPUTFORMAT 
  'org.apache.hadoop.mapred.TextInputFormat' 
OUTPUTFORMAT 
  'org.apache.hadoop.hive.ql.io.HiveIgnoreKeyTextOutputFormat'
LOCATION
  'hdfs://hadoop102:8020/warehouse/gmall/ods/ods_promotion_refer_full'
TBLPROPERTIES (
  'bucketing_version'='2', 
  'compression.codec'='org.apache.hadoop.io.compress.GzipCodec', 
  'transient_lastDdlTime'='1683720615')

-- 创建表：ods_refund_payment_inc
createtab_stmt
CREATE EXTERNAL TABLE `ods_refund_payment_inc`(
  `type` string COMMENT 'from deserializer', 
  `ts` bigint COMMENT 'from deserializer', 
  `data` struct<id:string,out_trade_no:string,order_id:string,sku_id:string,payment_type:string,trade_no:string,total_amount:decimal(16,2),subject:string,refund_status:string,create_time:string,callback_time:string,callback_content:string,operate_time:string> COMMENT 'from deserializer', 
  `old` map<string,string> COMMENT 'from deserializer')
COMMENT '退款表'
PARTITIONED BY ( 
  `dt` string)
ROW FORMAT SERDE 
  'org.apache.hadoop.hive.serde2.JsonSerDe' 
STORED AS INPUTFORMAT 
  'org.apache.hadoop.mapred.TextInputFormat' 
OUTPUTFORMAT 
  'org.apache.hadoop.hive.ql.io.HiveIgnoreKeyTextOutputFormat'
LOCATION
  'hdfs://hadoop102:8020/warehouse/gmall/ods/ods_refund_payment_inc'
TBLPROPERTIES (
  'bucketing_version'='2', 
  'compression.codec'='org.apache.hadoop.io.compress.GzipCodec', 
  'transient_lastDdlTime'='1683720617')

-- 创建表：ods_sku_attr_value_full
createtab_stmt
CREATE EXTERNAL TABLE `ods_sku_attr_value_full`(
  `id` string COMMENT '编号', 
  `attr_id` string COMMENT '平台属性ID', 
  `value_id` string COMMENT '平台属性值ID', 
  `sku_id` string COMMENT 'SKU_ID', 
  `attr_name` string COMMENT '平台属性名称', 
  `value_name` string COMMENT '平台属性值名称', 
  `create_time` string COMMENT '创建时间', 
  `operate_time` string COMMENT '修改时间')
COMMENT '商品平台属性表'
PARTITIONED BY ( 
  `dt` string)
ROW FORMAT SERDE 
  'org.apache.hadoop.hive.serde2.lazy.LazySimpleSerDe' 
WITH SERDEPROPERTIES ( 
  'field.delim'='\t', 
  'serialization.format'='\t', 
  'serialization.null.format'='') 
STORED AS INPUTFORMAT 
  'org.apache.hadoop.mapred.TextInputFormat' 
OUTPUTFORMAT 
  'org.apache.hadoop.hive.ql.io.HiveIgnoreKeyTextOutputFormat'
LOCATION
  'hdfs://hadoop102:8020/warehouse/gmall/ods/ods_sku_attr_value_full'
TBLPROPERTIES (
  'bucketing_version'='2', 
  'compression.codec'='org.apache.hadoop.io.compress.GzipCodec', 
  'transient_lastDdlTime'='1683720614')

-- 创建表：ods_sku_info_full
createtab_stmt
CREATE EXTERNAL TABLE `ods_sku_info_full`(
  `id` string COMMENT 'SKU_ID', 
  `spu_id` string COMMENT 'SPU_ID', 
  `price` decimal(16,2) COMMENT '价格', 
  `sku_name` string COMMENT 'SKU名称', 
  `sku_desc` string COMMENT 'SKU规格描述', 
  `weight` decimal(16,2) COMMENT '重量', 
  `tm_id` string COMMENT '品牌ID', 
  `category3_id` string COMMENT '三级品类ID', 
  `sku_default_img` string COMMENT '默认显示图片地址', 
  `is_sale` string COMMENT '是否在售', 
  `create_time` string COMMENT '创建时间', 
  `operate_time` string COMMENT '修改时间')
COMMENT '商品表'
PARTITIONED BY ( 
  `dt` string)
ROW FORMAT SERDE 
  'org.apache.hadoop.hive.serde2.lazy.LazySimpleSerDe' 
WITH SERDEPROPERTIES ( 
  'field.delim'='\t', 
  'serialization.format'='\t', 
  'serialization.null.format'='') 
STORED AS INPUTFORMAT 
  'org.apache.hadoop.mapred.TextInputFormat' 
OUTPUTFORMAT 
  'org.apache.hadoop.hive.ql.io.HiveIgnoreKeyTextOutputFormat'
LOCATION
  'hdfs://hadoop102:8020/warehouse/gmall/ods/ods_sku_info_full'
TBLPROPERTIES (
  'bucketing_version'='2', 
  'compression.codec'='org.apache.hadoop.io.compress.GzipCodec', 
  'transient_lastDdlTime'='1683720614')

-- 创建表：ods_sku_sale_attr_value_full
createtab_stmt
CREATE EXTERNAL TABLE `ods_sku_sale_attr_value_full`(
  `id` string COMMENT '编号', 
  `sku_id` string COMMENT 'SKU_ID', 
  `spu_id` string COMMENT 'SPU_ID', 
  `sale_attr_value_id` string COMMENT '销售属性值ID', 
  `sale_attr_id` string COMMENT '销售属性ID', 
  `sale_attr_name` string COMMENT '销售属性名称', 
  `sale_attr_value_name` string COMMENT '销售属性值名称', 
  `create_time` string COMMENT '创建时间', 
  `operate_time` string COMMENT '修改时间')
COMMENT '商品销售属性值表'
PARTITIONED BY ( 
  `dt` string)
ROW FORMAT SERDE 
  'org.apache.hadoop.hive.serde2.lazy.LazySimpleSerDe' 
WITH SERDEPROPERTIES ( 
  'field.delim'='\t', 
  'serialization.format'='\t', 
  'serialization.null.format'='') 
STORED AS INPUTFORMAT 
  'org.apache.hadoop.mapred.TextInputFormat' 
OUTPUTFORMAT 
  'org.apache.hadoop.hive.ql.io.HiveIgnoreKeyTextOutputFormat'
LOCATION
  'hdfs://hadoop102:8020/warehouse/gmall/ods/ods_sku_sale_attr_value_full'
TBLPROPERTIES (
  'bucketing_version'='2', 
  'compression.codec'='org.apache.hadoop.io.compress.GzipCodec', 
  'transient_lastDdlTime'='1683720614')

-- 创建表：ods_spu_info_full
createtab_stmt
CREATE EXTERNAL TABLE `ods_spu_info_full`(
  `id` string COMMENT 'SPU_ID', 
  `spu_name` string COMMENT 'SPU名称', 
  `description` string COMMENT '描述信息', 
  `category3_id` string COMMENT '三级品类ID', 
  `tm_id` string COMMENT '品牌ID', 
  `create_time` string COMMENT '创建时间', 
  `operate_time` string COMMENT '修改时间')
COMMENT 'SPU表'
PARTITIONED BY ( 
  `dt` string)
ROW FORMAT SERDE 
  'org.apache.hadoop.hive.serde2.lazy.LazySimpleSerDe' 
WITH SERDEPROPERTIES ( 
  'field.delim'='\t', 
  'serialization.format'='\t', 
  'serialization.null.format'='') 
STORED AS INPUTFORMAT 
  'org.apache.hadoop.mapred.TextInputFormat' 
OUTPUTFORMAT 
  'org.apache.hadoop.hive.ql.io.HiveIgnoreKeyTextOutputFormat'
LOCATION
  'hdfs://hadoop102:8020/warehouse/gmall/ods/ods_spu_info_full'
TBLPROPERTIES (
  'bucketing_version'='2', 
  'compression.codec'='org.apache.hadoop.io.compress.GzipCodec', 
  'transient_lastDdlTime'='1683720614')

-- 创建表：ods_user_info_inc
createtab_stmt
CREATE EXTERNAL TABLE `ods_user_info_inc`(
  `type` string COMMENT 'from deserializer', 
  `ts` bigint COMMENT 'from deserializer', 
  `data` struct<id:string,login_name:string,nick_name:string,passwd:string,name:string,phone_num:string,email:string,head_img:string,user_level:string,birthday:string,gender:string,create_time:string,operate_time:string,status:string> COMMENT 'from deserializer', 
  `old` map<string,string> COMMENT 'from deserializer')
COMMENT '用户表'
PARTITIONED BY ( 
  `dt` string)
ROW FORMAT SERDE 
  'org.apache.hadoop.hive.serde2.JsonSerDe' 
STORED AS INPUTFORMAT 
  'org.apache.hadoop.mapred.TextInputFormat' 
OUTPUTFORMAT 
  'org.apache.hadoop.hive.ql.io.HiveIgnoreKeyTextOutputFormat'
LOCATION
  'hdfs://hadoop102:8020/warehouse/gmall/ods/ods_user_info_inc'
TBLPROPERTIES (
  'bucketing_version'='2', 
  'compression.codec'='org.apache.hadoop.io.compress.GzipCodec', 
  'transient_lastDdlTime'='1683720618')

-- 创建表：tmp_dim_date_info
createtab_stmt
CREATE EXTERNAL TABLE `tmp_dim_date_info`(
  `date_id` string COMMENT '日', 
  `week_id` string COMMENT '周ID', 
  `week_day` string COMMENT '周几', 
  `day` string COMMENT '每月的第几天', 
  `month` string COMMENT '第几月', 
  `quarter` string COMMENT '第几季度', 
  `year` string COMMENT '年', 
  `is_workday` string COMMENT '是否是工作日', 
  `holiday_id` string COMMENT '节假日')
COMMENT '时间维度表'
ROW FORMAT SERDE 
  'org.apache.hadoop.hive.serde2.lazy.LazySimpleSerDe' 
WITH SERDEPROPERTIES ( 
  'field.delim'='\t', 
  'serialization.format'='\t') 
STORED AS INPUTFORMAT 
  'org.apache.hadoop.mapred.TextInputFormat' 
OUTPUTFORMAT 
  'org.apache.hadoop.hive.ql.io.HiveIgnoreKeyTextOutputFormat'
LOCATION
  'hdfs://hadoop102:8020/warehouse/gmall/tmp/tmp_dim_date_info'
TBLPROPERTIES (
  'bucketing_version'='2', 
  'transient_lastDdlTime'='1683979513')

